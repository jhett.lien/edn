let wWidth = $(window).width();


$(document).ready(function(){
    // 控制header 出現時機
    let lastScrollY = 0;
  $(window).scroll(function(){
      let nowScrollY = $(this).scrollTop();
      if(nowScrollY > 150 && nowScrollY > lastScrollY) {
        $('.money_header').addClass('hide');
        $('#toTop').css({'display':'block'});
      }
      else {
          $('.money_header').removeClass('hide');
          $('#toTop').css({'display':'none'});
      }
      lastScrollY = nowScrollY;
  });

  // 控制文章出現效果
  if(wWidth < 768) {
    $('.style_box').waypoint(function(){
        $(this).find('dt').addClass('showup');
     
    },{offset:'80%'});
  }
  

});



// 控制閱讀更多

$('.showmore').click(function(){
    let moreItem = $(this).siblings('dl').find('.style_box_wrap').find('dt').eq(2).html();
    $(this).siblings('dl').append('<dt style="opacity:1!important;">' + moreItem + '</dt>','<dt style="opacity:1!important;">' + moreItem + '</dt>','<dt style="opacity:1!important;">' + moreItem + '</dt>','<dt style="opacity:1!important;">' + moreItem + '</dt>','<dt style="opacity:1!important;">' + moreItem + '</dt>','<dt style="opacity:1!important;">' + moreItem + '</dt>');
    $(this).siblings('dl').find('.style_box_wrap').css({'justify-content':'space-between'});
    $(this).css('bottom','-30px');
    // $(this).siblings('dl').find('.style_box_wrap').find('dt').eq(0).css('margin','0 0 20px');
    // $(this).siblings('dl').find('.style_box_wrap').find('dt').eq(1).css('margin','0 0 20px');
    
    if(wWidth > 768) {
        $(this).parent('.style_box').css('margin','0 0 80px 0');
    }
});

// 控制回頂部動畫

$('#toTop').click(function(){
    $('html,body').animate({scrollTop:0},900);
});


// slick.js 
let options = {
    infinite:true,
    slidesToShow:3,
    slidesToScroll:1,
    autoplay:true,
    autoplaySpeed:6000,
    speed:1500,
    prevArrow:'<a href="" class="prev_arrow"><i class="icon-chevron-left"></i></a>',
    nextArrow:'<a href="" class="next_arrow"><i class="icon-chevron-right"></i></a>',
    responsive: [
        {
            breakpoint: 767,
            settings: "unslick"
        }
    ]

};
// $(window).resize(function(){
//     let style_box_wrap = $('.choice_box .title').offset().left;
//    console.log(style_box_wrap);
//     if(wWidth > 767) {
//         $('.money_header').css({'padding-left':style_box_wrap});
//         $('.money_header').css({'padding-right':style_box_wrap});
//     }
       

// });



// mmenu.js
$(document).ready(function(){
    $('#header_menu_area').mmenu({
        "extensions": [
            "position-right"
        ],
    });
});